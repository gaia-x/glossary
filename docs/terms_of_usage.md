# Terms of Usage

A specific instantiation of a data license included in a data product usage contract listing all the constraints associated with a data exchange.

## References
[Data Exchange Document 23.10](https://docs.gaia-x.eu/technical-committee/data-exchange/latest/dewg/) - 2.2  

## See Also
- [Data License](data_license.md)
- [Data Product Usage Contract](data_product_usage_contract.md)