# Enabling services

Enabling Services facilitate the operation of ecosystems. There are multiple technologies, products and implementations of each of the enabling services available. 

## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/enabling_services/#enabling-and-federation-services)- 7

## See Also
[Ecosystems](gaia-x_ecosystems.md)