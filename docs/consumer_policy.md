# Consumer Policy

Consumer Policy describes a consumer’s restriction of a requested Resource.

## Reference
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/component_details/#policy-description) - 4.2.2

## See Also
- [Consumer](consumer.md)
- [Resource](resource.md)
- [Policy](policy.md) 