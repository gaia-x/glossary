# Gaia-X Policy Rules

The Policy Rules or Gaia-X Policy Rules define high-level objectives safeguarding the added value and principles of the Gaia-X ecosystem. 

## References
[Gaia-X Compliance Document 24.11](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/Executive_Summary/#executive-summary)- 2

## See Also
[Gaia-X Ecosystem](gaia-x_ecosystem.md)
