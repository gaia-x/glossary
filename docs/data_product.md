# Data Product 

Data are furnished by Data Producers to Data Providers who compose them into a Data Product to be used by Data Consumers. 
A Data Product is described by a Data Product Description, which must be a valid Gaia-X Credential and is stored in a (searchable) Federated Data Catalogue.

## References
[Gaia-X Architecture Document 25.XX - ongoing version](https://gaia-x.gitlab.io/technical-committee/architecture-working-group/architecture-document/component_details/#data-product-conceptual-model) - 4.5.1

## See Also
- [Data Producer](data_producer.md)
- [Data Provider](data_provider.md)
- [Data Product Description](data_product_description.md)
