# Gaia-X Credential

A Gaia-X credential is a verifiable credential (VC) using the Gaia-X Ontology which is available via the Gaia-X Registry.
A holder can put several Gaia-X credentials together to build a verifiable presentation.

## References
[Gaia-X Architecture Document 24.04](https://gaia-x.gitlab.io/technical-committee/architecture-working-group/architecture-document/operating_model/#gaia-x-credentials-and-attestations) - 5.1.2.2

## See Also 
- [Verifiable Credential (VC)](verifiable_credential.md)
- [Gaia-X Ontology](gaia-x_ontology.md])
- [Gaia-X Registry](gaia-x.registry-md)
- [holder](holder.md)
- [Verifiable Presentation (VP)](https://www.w3.org/TR/vc-data-model-2.0/#presentations)