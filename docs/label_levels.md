# Label levels

Gaia-X distinguishes 3 levels of Labels, starting from Label Level 1 (the lowest), up to Label Level 3 (the highest), which represent different degrees of compliance with regard to the goals of transparency, autonomy, data protection, security, interoperability, portability, sustainability, and European Control.

## References
[Gaia-X Compliance Document 24.11](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#european-control)- 4.7

