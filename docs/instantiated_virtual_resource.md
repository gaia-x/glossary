# Instantiated Virtual Resource

An Instantiated Virtual Resource represents an instance of a virtual resource. It is equivalent to a service instance and is characterized by endpoints and access rights.

## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/component_details/#resources-and-service-offerings) 4.1

## See Also
- [Virtual Resource](virtual_resource.md)
- [Service Instance](service_instance.md)
