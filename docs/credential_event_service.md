# Credential Event Service (CES)

CES is a common publication/subscription service that allows the Gaia-X catalogues of the GXDCH to be notified about new, updated, and revoked credentials.


## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/gx_services/#gaia-x-credential-event-service-ces) - 6.4


## See Also
- [Gaia-X Catalogues](catalogue.md)
- [GXDCH](GXDCH.md)