# Customer Data

In the context of the definition of Gaia-X Policy Rules and Labelling Criteria for Cloud Services, the term is used for all customer provided or generated data, both personal and non-personal data, as processed by a Provider.

## References
[Gaia-X Policy Compliance Document 24.11](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#gaia-x-compliance-criteria-for-cloud-services) - 4
