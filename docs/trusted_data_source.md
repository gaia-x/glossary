# Trusted Data Source

Source of the information used by the issuer to validate attestations.
Notaries perform validations and issue attestations based on objective evidences from Trusted Data sources. 
The accepted Trusted Data Source categories and Notaries are determined within the Gaia-X Compliance document, while the detailed list of valid Trusted Data Sources and Notaries resides in the Gaia-X Registry.

## References
[Gaia-X Compliance Document 24.11](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/Gaia-X_Trust_Anchors/#trusted-data-sources-and-notaries)- 6.3

## See Also
[Evidences](evidence.md)
