# NIST planes

The three planes Trust plane, Management plane, and Usage plane. They represent three levels of interoperability, as described in the NIST Cloud Federation Reference Architecture.

## References
- [NIST Cloud Federation Reference Architecture](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.500-332.pdf)
- [Gaia-X Architecture Document 23.10](https://docs.gaia-x.eu/technical-committee/architecture-document/23.10/) - 3.1.2
