# Entity

Entity is an item relevant for the purpose of operation of a domain that has recognizably distinct existence.

Note 1 to entry: An entity can have a physical or a logical embodiment.

## References
[ISO/IEC 24760-1:2019](https://www.iso.org/obp/ui/en/#iso:std:iso-iec:24760:-1:ed-2:v1:en)
