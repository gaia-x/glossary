# Data Rights Holder

A natural or legal participant (not necessarily a Gaia-X participant) who own usage rights for some data. It can be a data subject as per GDPR for personal data or a primary owner of non-personal data (i.e. not liable to GDPR).

## References
[Data Exchange Document 25.XX - in progress](https://gitlab.com/gaia-x/technical-committee/data-exchange-working-group/data-exchange/-/blob/main/specs/dewg.md?ref_type=heads) 
