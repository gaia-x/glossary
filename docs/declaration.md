# Declaration

First-party attestation.

## References
[ISO/IEC 17000:2020](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.3)

## See Also
[Attestation](attestation.md)