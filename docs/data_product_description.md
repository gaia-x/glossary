# Data Product Description 

Data Products are described by a Data Product Description, which must be a valid Gaia-X Credential.

## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/component_details/#data-product-conceptual-model) - 4.5.1

## See Also
[Data Product](data_product.md) 