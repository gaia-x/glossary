# Physical Resource

A physical resource is a resource that has a weight, position in space and represents a physical entity that hosts, manipulates, or interacts with other physical entities.

## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/component_details/#resources-and-service-offerings) - 4.1

## See Also
[Resource](resource.md)