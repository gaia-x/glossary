# Data License 

Each data product description contains a Data License defining the usage policies for all data in the data product. 

## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/component_details/#data-product-conceptual-model) - 4.5.1

## See Also
- [Data Product Description](data_product_description.md)
- [Data Product](data_product.md)
