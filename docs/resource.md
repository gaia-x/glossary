# Resource

Resources describe in general the goods and objects of the [Gaia-X Ecosystem](gaia-x_ecosystem.md). A Resource can be a:

- [Physical Resource](physical_resource.md)
- [Virtual Resource](virtual_resource.md)
- [Instantiated Virtual Resource](instantiated_virtual_resource.md)

## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/component_details/#resources-and-service-offerings)- 4.1
