# Virtual Resource

A Virtual Resource represents static data in any form and necessary information such as dataset, configuration file, license, keypair, an AI model, neural network weights, etc.

## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/component_details/#resources-and-service-offerings) - 4.1

## See Also
[Resource](resource.md)