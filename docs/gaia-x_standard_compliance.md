# Gaia-X Standard Compliance

The Gaia-X Standard Compliance level defines the minimal set of requirements to be able to participate in the Gaia-X ecosystem. The optional Label levels define additional criteria and conformance-ensuring measures such as certificates, to achieve additional levels of assurance and trust.

## References
[Gaia-X Compliance Document 24.11](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/Executive_Summary/)- 2

## See Also
[Gaia-X Ecosystem](gaia-x_ecosystem.md)