# Evidence

Evidence can be included by an issuer to provide the verifier with additional supporting information in a verifiable credential. This could be used by the verifier to establish the confidence with which it relies on the claims in the verifiable credential.

It is expected that the credentials issued by the notaries contain the evidence of the validation process.

## References
- [W3C, Verifiable Credentials Data Model v2.0](https://www.w3.org/TR/vc-data-model-2.0/#evidence)
- [Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/component_details/#evidence)- 4.6.1.1

## See Also
- [Issuer](issuer.md)
- [verifier](verifier.md)
- [verifiable credential](verifiable_credential.md)
- [Notaries](notary.md)