# Gaia-X Compliance Service

The service takes as input the Verifiable Presentations provided by the participants, checks them against the SHACL Shapes available in the Gaia-X Registry and performs other consistency checks based on the Gaia-X Policy Rules.
The service returns a Verifiable Credential, the “Gaia-X Compliance Credential” with a Gaia-X signature, as a proof that the input provided has passed all the verifications.

## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/gx_services/#gaia-x-compliance)- 6.2

## See Also
- [Verifiable Presentation](verifiable_presentation.md)
- [Verifiable Credential](verifiable_credential.md)
