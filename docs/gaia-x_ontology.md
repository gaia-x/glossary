# Gaia-X Ontology

An ontology is a formal, explicit specification of a shared conceptualisation (Gruber,1993).

In Gaia-X case, it means to create models that are understandable by an algorithm so one can automate rules with a computer. The models are developed by Gaia-X members under the Technical Committee.


## References
- [Gaia-X Compliance Document 24.06](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#assessment-procedures)- 4.2
- Gaia-X Ontology: https://docs.gaia-x.eu/ontology/development/
