# Policy

Policy is defined as a statement of objectives, rules, practices, or regulations governing the activities
of Participants within Gaia-X. From a technical perspective, Policies are statements, rules or
assertions that specify the correct or expected behaviour of an entity.

## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/component_details/#policy-definition) - 4.2.1

