# Conformity Assessment Body (CAB)

Conformity Assessment Body (CAB) performs Conformity Assessment services. Conformity with label criteria can be declared by self-assessment (declaration) or supported by external Conformity Assessment Bodies (CAB). The Gaia-X Association reserves its right to choose its own CABs for its three basic labels.

## References
- [ISO/IEC 17000:2020](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:6.5)
- [Gaia-X Policy Rules Conformity Document](https://docs.gaia-x.eu/policy-rules-committee/policy-rules-conformity-document/latest/) - 3.2.4