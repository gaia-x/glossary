# Data Usage Agreement (DUA)

The Data Usage Agreement is signed by the data rights holder and by the data consumer and gives to the Data Consumer the legal authorization to use the data in accordance with the constraints specified by the Data Rights Holder and to the Data Rights Holder the assurance that the Data Consumer commits to respect these constraints.

## References
[Gaia-X Architecture Document 25.XX - ongoing version](https://gaia-x.gitlab.io/technical-committee/architecture-working-group/architecture-document/component_details/#data-product-conceptual-model) - 4.5.1

## See Also
- [Data Rights Holder](data_rights_holder.md)
- [Data Consumer](data_consumer.md)