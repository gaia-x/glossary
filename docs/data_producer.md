# Data Producer

A natural or legal participant who furnishes data to a data product provider.

## References
- [Data Exchange Document 23.10](https://docs.gaia-x.eu/technical-committee/data-exchange/latest/dewg/) - 2.2 
- [Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/component_details/#data-product-conceptual-model) 4.5.1

## See Also
- [Participant](participant.md)
- [Data Product Provider](data_product_provider.md)