# Gaia-X Member

A Gaia-X member is a member of the international non-profit organization, Gaia-X.

## References
[Gaia-X website](https://gaia-x.eu/join-gaia-x/)

## See Also
[Gaia-X](gaia_x.md)